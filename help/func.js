const fdk=require('@autom8/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  let name = 'World';
  if (input.name) {
    name = input.name;
  }
  return {'message': 'Haaaaaaaalp!'}
})


fdk.slack(function(result){
	return {
    "response_type": "in_channel",
    "blocks" : [{
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "🗣 haaaaaaaaaalp!! Don't worry, we got you 💪\n👉  Basic command structure: `/a8 namespace.function param:value, param:value`\n  *↝*    Named parameters are optional and depend on whether the function you're calling takes parameters\n  *↝*    For functions that take a string of text you can just type the text\n\nYou can write your own function that's immediately available wherever a8 is integrated. It will literally take you minutes, not hours. <https://slack.a8.dev|Learn how>!"
      }
    }]
  }
})


fdk.discord(function(result){
  return {
    "content" : result.message + " in discord form!",
    "embed": {
      "title": ":speaking_head_in_silhouette: haaaaaaaaaalp!! Don't worry, we got you :muscle:",
      "description": "*↝* Basic command structure: `a8! namespace.function param:value, param:value`\n  *↝*    Named parameters are optional and depend on whether the function you're calling takes parameters\n*↝* For functions that take a string of text you can just type the text\n\nYou can write your own function that's immediately available wherever a8 is integrated. It will literally take you minutes, not hours. [Learn how!](https://discord.a8.dev/)!"
    }
  }
})
