'use strict'

const parseFn = require('../parseFn.js')
const assert = require('assert')

describe('parseFn', () => {
    
    it('should return {"message":"no function","result":"error"} when no fqn or params are given', () => {
        const parsed = parseFn("")

        assert.equal(parsed.message === 'no function', true)
        assert.equal(parsed.result === 'error', true)
        
        assert.equal(parsed.params === undefined, true)
        assert.equal(parsed.fqn === undefined, true)
    })

    it('should return { result: "error", message: "invalid fqn" } when given "a8"', () => {
        const parsed = parseFn(`a8`)

        assert.equal(parsed.message === "invalid fqn", true)
        assert.equal(parsed.result === "error", true)
        
        assert.equal(parsed.params === undefined, true)
        assert.equal(parsed.fqn === undefined, true)
    })

    it('should return { result: "success", fqn: "a8.dev" } when given "a8.dev"', () => {
        const parsed = parseFn("a8.dev")

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "a8.dev", true)
        
        assert.equal(parsed.params === undefined, true)
    })

    it('should return { result: "success", fqn: "a8.dev.test" } when given "a8.dev.test"', () => {
        const parsed = parseFn("a8.dev.test")

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "a8.dev.test", true)
        
        assert.equal(parsed.message === undefined, true)
        assert.equal(parsed.params === undefined, true)
    })

    it('should return { \
        result: "success", \
        fqn: "a8.dev.test", \
        params: { _inputs: [ "ok" ] } } when given "a8.dev.test ok"', () => {
        const parsed = parseFn("a8.dev.test ok")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "a8.dev.test", true)
        assert.equal(params === undefined, false)
        assert.equal(Array.isArray(params._inputs), true)
        assert.equal(params._inputs.length === 1, true)
        assert.equal(params._inputs[0] === "ok", true)
        
        assert.equal(parsed.message === undefined, true)
    })

    it('should return { \
        result: "success", \
        fqn: "a8.dev.test", \
        params: { ok: "ok" } } when given "a8.dev.test ok:ok"', () => {
        const parsed = parseFn("a8.dev.test ok:ok")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "a8.dev.test", true)
        assert.equal(params === undefined, false)
        assert.equal(params.ok === "ok", true)
    })

    it('should return { \
        result: "success", \
        fqn: "a8.dev.test", \
        params: { ok: 1 } } when given "a8.dev.test ok:1"', () => {
        const parsed = parseFn("a8.dev.test ok:1")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "a8.dev.test", true)
        assert.equal(params === undefined, false)
        assert.equal(params.ok === 1, true)
    })

    it('should return { \
        result: "success", \
        fqn: "a8.dev.test", \
        params: { ok: "[1]" } } when given "a8.dev.test ok:[1]"', () => {
        const parsed = parseFn("a8.dev.test ok:[1]")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "a8.dev.test", true)
        assert.equal(params === undefined, false)
        assert.equal(params.ok === "[1]", true)
    })

    it('should return { \
        result: "success", \
        fqn: "crypto_eth.get_address_balance", \
        params: { address: "0x87BBb9d32C5a10F3129E63cD96349342Fcbd5972" } } \
        when given crypto_eth.get_address_balance address:0x87BBb9d32C5a10F3129E63cD96349342Fcbd5972', () => {
        const parsed = parseFn("crypto_eth.get_address_balance address:0x87BBb9d32C5a10F3129E63cD96349342Fcbd5972")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "crypto_eth.get_address_balance", true)
        assert.equal(params === undefined, false)
        assert.equal(params.address === "0x87BBb9d32C5a10F3129E63cD96349342Fcbd5972", true)
    })

    it('should return { \
        result: "success", \
        fqn: "crypto_btc.get_address_balance", \
        params: { address: "3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r" } } \
        when given crypto_eth.get_address_balance address:3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r', () => {
        const parsed = parseFn("crypto_btc.get_address_balance address:3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "crypto_btc.get_address_balance", true)
        assert.equal(params === undefined, false)
        assert.equal(params.address === "3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r", true)
    })

    it('it should return { \
        result: "success", \
        fqn: "this.is.a.test", \
        params: { param: 1, param2: "two" } } \
        when given this.is.a.test param1:1, param2:two', () => {
        const parsed = parseFn("this.is.a.test param1:1, param2:two")
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "this.is.a.test", true)
        assert.equal(params === undefined, false)
        assert.equal(params.param1 === 1, true)
        assert.equal(params.param2 === "two", true)
    })

    // todo: We should add support for this later
    /* 
    it('it should return { \
        result: "success", \
        fqn: "this.is.a.test", \
        params: { param1: 1, param2: "2" } } \
        when given this.is.a.test param:1, param2:two', () => {
        const parsed = parseFn('this.is.a.test param1:1, param2:"2"')
        const params = parsed.params

        assert.equal(parsed.result === "success", true)
        assert.equal(parsed.fqn === "this.is.a.test", true)
        assert.equal(params === undefined, false)
        assert.equal(params.param1 === 1, true)
        assert.equal(params.param2 === "2", true)
    })
    */
})