'use strict'

const parseFn = (funcAndParams) => {
    var resultObj = {}
    const funcAndParamsTrimmed = funcAndParams.trim()
    if (funcAndParamsTrimmed.length === 0) {
      resultObj.message = "no function"
      resultObj.result = "error"

      return resultObj
    }

    const fqnAndParams = funcAndParamsTrimmed.split(/ (.+)/)
    if (fqnAndParams.length === 0) {
      return {"result": "error", "message":"invalid fqn"}
    }

    let fqn = fqnAndParams[0]

    // if the command doesn't contain a dot it is a call to the a8 namespace and should be handled as such
    if (!fqn.includes('.')) {
      fqn = `a8.${fqn}`
    }

    if (fqnAndParams.length === 1) {
        resultObj.result = "success"
        resultObj.fqn = fqn
        return resultObj
    }

    const paramsAndVals = fqnAndParams[1].split(',')
    // split at colons
    const paramTuples = paramsAndVals.map(paramWithVal => paramWithVal.split(':'))

    //if there is more than one colon per tuple, that's bad
    if(paramTuples.filter(paramTuple => paramTuple.length > 2 && paramTuple.length !== 0 ).length > 0) {
      resultObj.result = "error"
      resultObj.message = "one or more param formatted incorrectly"
    }

    var paramValObj = {}
    var _inputArray = []
    paramTuples.forEach((tuple) => {

      if(tuple.length  === 2 ) {
        paramValObj[tuple[0].trim()] = formatValue(tuple[1])
      }else{
        _inputArray.push(formatValue(tuple[0]))
      }
    })

    if (_inputArray.length > 0) {
      paramValObj._inputs = _inputArray
    }


    resultObj.result = "success"
    resultObj.fqn = fqn
    resultObj.params = paramValObj

    return resultObj
  }

const formatValue = (strVal) => {
  const numVal = Number(strVal)
  let converted

  if (strVal.startsWith('0x')) {
    converted = strVal
  } else if (isNaN(numVal)) {
    converted = strVal
  } else {
    converted = numVal
  }

  return converted
}

module.exports = parseFn
