const fdk = require('@fnproject/fdk');
const a8 = require('@autom8/js-a8-fdk')
const parseFn = require('./parseFn.js')

fdk.handle(function(input){
  if (input.fn) {
    return parseFn(input.fn)
  } else {
    return {"message":"missing fn field"}
  }
})
